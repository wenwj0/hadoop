# hadoop

#### 介绍
Hadoop是一个用Java编写的Apache开源框架，允许使用简单的编程模型跨计算机集群分布式处理大型数据集。Hadoop框架工作的应用程序在跨计算机集群提供分布式存储和计算的环境中工作。Hadoop旨在从单个服务器扩展到数千个机器，每个都提供本地计算和存储。

#### 软件架构
Hadoop框架包括以下四个模块：
- Hadoop Common: 这些是其他Hadoop模块所需的Java库和实用程序。这些库提供文件系统和操作系统级抽象，并包含启动Hadoop所需的Java文件和脚本。
- Hadoop YARN: 这是一个用于作业调度和集群资源管理的框架。
- Hadoop Distributed File System (HDFS): 分布式文件系统，提供对应用程序数据的高吞吐量访问。
- Hadoop MapReduce：这是基于YARN的用于并行处理大数据集的系统。
自2012年以来，“Hadoop”这个术语通常不仅指上述基本模块，而且还指向可以安装在Hadoop之上或之上的附加软件包的收集，例如Apache Pig，Apache Hive，Apache HBase，Apache Spark等。

#### ARM支持：

1. 移植指南：https://support.huaweicloud.com/prtg-apache-kunpengbds/kunpenghadoop_02_0001.html
2. 部署指南：https://support.huaweicloud.com/dpmg-apache-kunpengbds/kunpenghadoop_04_0001.html
3. 调优指南：XXX

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
