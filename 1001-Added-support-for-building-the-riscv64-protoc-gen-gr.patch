From 78df1e23afdb5b4f53004a807f9ffa9ac13be2e1 Mon Sep 17 00:00:00 2001
From: Dingli Zhang <dingli@iscas.ac.cn>
Date: Mon, 29 Apr 2024 16:52:34 +0800
Subject: [PATCH] Added support for building the riscv64 protoc-gen-grpc-java
 binaries with Kokoro


diff --git a/build.gradle b/build.gradle
index db7be15f4..7c796341f 100644
--- a/build.gradle
+++ b/build.gradle
@@ -20,6 +20,8 @@ subprojects {
     version = "1.26.0" // CURRENT_GRPC_VERSION
 
     repositories {
+        maven { url 'file://@HOME@/.m2/repository'}
+        maven { url "https://mirrors.huaweicloud.com/repository/maven/"}
         maven { // The google mirror is less flaky than mavenCentral()
             url "https://maven-central.storage-download.googleapis.com/repos/central/data/" }
         mavenCentral()
diff --git a/buildscripts/kokoro/linux_artifacts.sh b/buildscripts/kokoro/linux_artifacts.sh
index cdf360762..b3c055e9e 100755
--- a/buildscripts/kokoro/linux_artifacts.sh
+++ b/buildscripts/kokoro/linux_artifacts.sh
@@ -37,3 +37,7 @@ cp -r "$LOCAL_MVN_TEMP"/* "$MVN_ARTIFACT_DIR"/
 # for aarch64 platform
 sudo apt-get install -y g++-aarch64-linux-gnu
 SKIP_TESTS=true ARCH=aarch_64 "$GRPC_JAVA_DIR"/buildscripts/kokoro/unix.sh
+
+# for riscv64 platform
+sudo apt-get install -y g++-riscv64-linux-gnu
+SKIP_TESTS=true ARCH=riscv64 "$GRPC_JAVA_DIR"/buildscripts/kokoro/unix.sh
diff --git a/buildscripts/kokoro/unix.sh b/buildscripts/kokoro/unix.sh
index c64bc7e22..48e36ce22 100755
--- a/buildscripts/kokoro/unix.sh
+++ b/buildscripts/kokoro/unix.sh
@@ -9,6 +9,8 @@
 #  ARCH=x86_32 ./buildscripts/kokoro/unix.sh
 # For aarch64 arch:
 #  ARCH=aarch_64 ./buildscripts/kokoro/unix.sh
+# For riscv64 arch:
+#  ARCH=riscv64 ./buildscripts/kokoro/unix.sh
 
 # This script assumes `set -e`. Removing it may lead to undefined behavior.
 set -exu -o pipefail
@@ -36,7 +38,7 @@ GRADLE_FLAGS+=" -PtargetArch=$ARCH"
 GRADLE_FLAGS+=" -Pcheckstyle.ignoreFailures=false"
 GRADLE_FLAGS+=" -PfailOnWarnings=true"
 GRADLE_FLAGS+=" -PerrorProne=true"
-GRADLE_FLAGS+=" -Dorg.gradle.parallel=true"
+GRADLE_FLAGS+=" -Dorg.gradle.parallel=false"
 export GRADLE_OPTS="-Xmx512m"
 
 # Make protobuf discoverable by :grpc-compiler
@@ -72,7 +74,7 @@ fi
 LOCAL_MVN_TEMP=$(mktemp -d)
 # Note that this disables parallel=true from GRADLE_FLAGS
 if [[ -z "${ALL_ARTIFACTS:-}" ]]; then
-  if [[ $ARCH == "aarch_64" ]]; then
+  if [[ $ARCH == "aarch_64" || $ARCH == "riscv64" ]]; then
     GRADLE_FLAGS+=" -x grpc-compiler:generateTestProto -x grpc-compiler:generateTestLiteProto"
     GRADLE_FLAGS+=" -x grpc-compiler:testGolden -x grpc-compiler:testLiteGolden"
     GRADLE_FLAGS+=" -x grpc-compiler:testDeprecatedGolden -x grpc-compiler:testDeprecatedLiteGolden"
diff --git a/buildscripts/kokoro/upload_artifacts.sh b/buildscripts/kokoro/upload_artifacts.sh
index c3c97444d..215573e5e 100644
--- a/buildscripts/kokoro/upload_artifacts.sh
+++ b/buildscripts/kokoro/upload_artifacts.sh
@@ -30,6 +30,9 @@ LOCAL_MVN_ARTIFACTS="$KOKORO_GFILE_DIR"/github/grpc-java/mvn-artifacts/
 # for linux aarch64 platform
 [[ "$(find "$LOCAL_MVN_ARTIFACTS" -type f -iname 'protoc-gen-grpc-java-*-linux-aarch_64.exe' | wc -l)" != '0' ]]
 
+# for linux riscv64 platform
+[[ "$(find "$LOCAL_MVN_ARTIFACTS" -type f -iname 'protoc-gen-grpc-java-*-linux-riscv64.exe' | wc -l)" != '0' ]]
+
 # from macos job:
 [[ "$(find "$LOCAL_MVN_ARTIFACTS" -type f -iname 'protoc-gen-grpc-java-*-osx-x86_64.exe' | wc -l)" != '0' ]]
 
diff --git a/buildscripts/make_dependencies.sh b/buildscripts/make_dependencies.sh
index 92ef9d7f7..9b5441d5b 100755
--- a/buildscripts/make_dependencies.sh
+++ b/buildscripts/make_dependencies.sh
@@ -7,8 +7,8 @@ PROTOBUF_VERSION=3.11.0
 
 # ARCH is x86_64 bit unless otherwise specified.
 ARCH="${ARCH:-x86_64}"
-DOWNLOAD_DIR=/tmp/source
-INSTALL_DIR="/tmp/protobuf-cache/$PROTOBUF_VERSION/$(uname -s)-$ARCH"
+DOWNLOAD_DIR=/var/tmp/source
+INSTALL_DIR="/var/tmp/protobuf-cache/$PROTOBUF_VERSION/$(uname -s)-$ARCH"
 mkdir -p $DOWNLOAD_DIR
 
 # Start with a sane default
@@ -36,6 +36,8 @@ else
       --prefix="$INSTALL_DIR"
   elif [[ "$ARCH" == aarch* ]]; then
     ./configure --disable-shared --host=aarch64-linux-gnu --prefix="$INSTALL_DIR"
+  elif [[ "$ARCH" == riscv* ]]; then
+    ./configure --disable-shared --host=riscv64-linux-gnu --prefix="$INSTALL_DIR"
   fi
   # the same source dir is used for 32 and 64 bit builds, so we need to clean stale data first
   make clean
diff --git a/compiler/build.gradle b/compiler/build.gradle
index 098f48c30..932252a01 100644
--- a/compiler/build.gradle
+++ b/compiler/build.gradle
@@ -55,6 +55,7 @@ model {
                 cppCompiler.executable = 'aarch64-linux-gnu-g++'
                 linker.executable = 'aarch64-linux-gnu-g++'
             }
+            target("riscv64")
             target("s390_64")
         }
         clang(Clang) {
@@ -66,6 +67,7 @@ model {
         x86_64 { architecture "x86_64" }
         ppcle_64 { architecture "ppcle_64" }
         aarch_64 { architecture "aarch_64" }
+        riscv64 { architecture "riscv64" }
         s390_64 { architecture "s390_64" }
     }
 
@@ -76,6 +78,7 @@ model {
                 'x86_64',
                 'ppcle_64',
                 'aarch_64',
+                'riscv64',
                 's390_64'
             ]) {
                 // If arch is not within the defined platforms, we do not specify the
diff --git a/compiler/check-artifact.sh b/compiler/check-artifact.sh
index 13ae89c74..e48ec227d 100755
--- a/compiler/check-artifact.sh
+++ b/compiler/check-artifact.sh
@@ -55,12 +55,15 @@ checkArch ()
     format="$(objdump -f "$1" | grep -o "file format .*$" | grep -o "[^ ]*$")"
     echo Format=$format
     if [[ "$OS" == linux ]]; then
+      host_machine="$(uname -m)";
       if [[ "$ARCH" == x86_32 ]]; then
         assertEq "$format" "elf32-i386" $LINENO
       elif [[ "$ARCH" == x86_64 ]]; then
         assertEq "$format" "elf64-x86-64" $LINENO
       elif [[ "$ARCH" == aarch_64 ]]; then
         assertEq "$format" "elf64-little" $LINENO
+      elif [[ "$ARCH" == riscv64 ]]; then
+        assertEq $format "elf64-littleriscv" $LINENO
       else
         fail "Unsupported arch: $ARCH"
       fi
@@ -108,6 +111,9 @@ checkDependencies ()
     elif [[ "$ARCH" == aarch_64 ]]; then
       dump_cmd='aarch64-linux-gnu-objdump -x '"$1"' |grep "NEEDED"'
       white_list="linux-vdso\.so\.1\|libpthread\.so\.0\|libm\.so\.6\|libc\.so\.6\|ld-linux-aarch64\.so\.1"
+    elif [[ "$ARCH" == riscv64 ]]; then
+      dump_cmd='objdump -p '"$1"' | grep NEEDED'
+      white_list="libm\.so\.6\|libc\.so\.6\|ld-linux-riscv64-lp64d\.so\.1"
     fi
   elif [[ "$OS" == osx ]]; then
     dump_cmd='otool -L '"$1"' | fgrep dylib'
diff --git a/settings.gradle b/settings.gradle
index 3f9a7e6ef..9f1c058ae 100644
--- a/settings.gradle
+++ b/settings.gradle
@@ -2,7 +2,7 @@ pluginManagement {
     plugins {
         id "com.github.johnrengelman.shadow" version "2.0.4"
         id "com.github.kt3k.coveralls" version "2.0.1"
-        id "com.google.osdetector" version "1.4.0"
+        id "com.google.osdetector" version "1.7.2"
         id "com.google.protobuf" version "0.8.8"
         id "me.champeau.gradle.japicmp" version "0.2.5"
         id "me.champeau.gradle.jmh" version "0.4.5"
-- 
2.41.0

